
CREATE TABLE A (val INTEGER PRIMARY KEY);
CREATE TABLE B (val INTEGER PRIMARY KEY);
CREATE TABLE C (val INTEGER PRIMARY KEY);

INSERT INTO A VALUES (1);
INSERT INTO A VALUES (2);
INSERT INTO A VALUES (3);

INSERT INTO B VALUES (1);
INSERT INTO B VALUES (3);

\echo
--Problem 1
\echo PROBLEM 1 '\n'----------------------------------------------
--(a)
\echo 1 (a)
SELECT 	NOT EXISTS(SELECT * FROM ((SELECT * FROM A) EXCEPT (SELECT * FROM B)) _) as empty_a_minus_b,
	NOT EXISTS(SELECT * FROM ((SELECT * FROM B) EXCEPT (SELECT * FROM A)) _) as empty_b_minus_a,
	NOT EXISTS(SELECT * FROM ((SELECT * FROM B) INTERSECT (SELECT * FROM A)) _) as empty_a_intersection_b;

--(b)
\echo 1 (b)
SELECT 	NOT EXISTS(SELECT * FROM (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)) _) as empty_a_minus_b,
	NOT EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val not in (SELECT * FROM A)) _) as empty_b_minus_a,
	NOT EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val in (SELECT * FROM A)) _) as empty_a_intersection_b;

\echo
--Problem 2
\echo PROBLEM 2 '\n'----------------------------------------------
--(a)
\echo 2 (a)
SELECT 	EXISTS(SELECT * FROM ((SELECT * FROM A) INTERSECT (SELECT * FROM B)) _) as answer;
SELECT 	EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val in (SELECT * FROM A)) _) as answer;

--(b)
\echo 2 (b)
SELECT 	NOT EXISTS(SELECT * FROM ((SELECT * FROM A) INTERSECT (SELECT * FROM B)) _) as answer;
SELECT 	NOT EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val in (SELECT * FROM A)) _) as answer;

--(c)
\echo 2 (c)
SELECT 	NOT EXISTS(SELECT * FROM ((SELECT * FROM A) EXCEPT (SELECT * FROM B)) _) as answer;
SELECT 	NOT EXISTS(SELECT * FROM (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)) _) as answer;

--(d)
\echo 2 (d)
SELECT 	EXISTS(SELECT * FROM ((SELECT * FROM B) EXCEPT (SELECT * FROM A)) _) as answer;
SELECT 	EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val not in (SELECT * FROM A)) _) as answer;

--(e)
\echo 2 (e)
SELECT 	(EXISTS(SELECT * FROM ((SELECT * FROM B) EXCEPT (SELECT * FROM A)) _) OR
	EXISTS(SELECT * FROM ((SELECT * FROM A) EXCEPT (SELECT * FROM B)) _)) as answer;

SELECT 	(EXISTS(SELECT * FROM (SELECT * FROM B b WHERE b.val not in (SELECT * FROM A)) _) OR
	EXISTS(SELECT * FROM (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)) _)) as answer;

--(f)
\echo 2 (f)

SELECT NOT EXISTS(
		(SELECT *
		FROM 	((SELECT *
				FROM ((SELECT * FROM A) EXCEPT (SELECT * FROM B)) a1)
				EXCEPT
				(SELECT *
				FROM A a1
				WHERE a1.val >= ALL ((SELECT * FROM A) EXCEPT (SELECT * FROM B)))) a)
		
		EXCEPT

		(SELECT *
		FROM A a2
		WHERE a2.val >= ALL(
				(SELECT *
				FROM ((SELECT * FROM A) EXCEPT (SELECT * FROM B)) a1)
				EXCEPT
				(SELECT *
				FROM A a1
				WHERE a1.val >= ALL ((SELECT * FROM A) EXCEPT (SELECT * FROM B)))
				)
		)) as answer;


SELECT NOT EXISTS(
		SELECT *
		FROM 	(SELECT *
			FROM (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)) a1
			WHERE a1.val NOT IN
			(SELECT *
			FROM A a1
			WHERE a1.val >= ALL (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)))) a2
		
		WHERE a2.val not in

		(SELECT *
		FROM A a2
		WHERE a2.val >= ALL(
				SELECT *
				FROM (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)) a1
				WHERE a1.val NOT IN
				(SELECT *
				FROM A a1
				WHERE a1.val >= ALL (SELECT * FROM A a WHERE a.val not in (SELECT * FROM B)))
				)
		)) as answer;


--(g)
\echo 2 (g)
SELECT	EXISTS(SELECT * FROM ((SELECT * FROM C) EXCEPT ((SELECT * FROM B) INTERSECT (SELECT * FROM A))) _) as answer;
SELECT	EXISTS(SELECT * FROM (SELECT * FROM C c where c.val not in (SELECT * FROM B b where b.val in (SELECT * FROM A))) _) as answer;

--(h)
\echo 2 (h)

SELECT COUNT(*) = 2 as answer FROM (((SELECT * FROM B) UNION (SELECT * FROM C)) INTERSECT (SELECT * FROM A)) _;




SELECT CC.count + BC.count = 2 as answer from 	
	(SELECT COUNT(*) FROM C c WHERE (c.val not in (SELECT * FROM B)) and (c.val in (SELECT * FROM A))) CC,
	(SELECT COUNT(*) FROM B b WHERE (b.val not in (SELECT * FROM C)) and (b.val in (SELECT * FROM A))) BC;

DROP TABLE A CASCADE;
DROP TABLE C CASCADE;
DROP TABLE B CASCADE;

\echo
--Problem 3
\echo PROBLEM 3 '\n'----------------------------------------------
CREATE TABLE P (coefficient INTEGER, degree INTEGER PRIMARY KEY);
CREATE TABLE Q (coefficient INTEGER, degree INTEGER PRIMARY KEY);

insert into P  values (2,2), (-5, 1), (5, 0);
insert into Q  values (3,3), (1,2), (-1,1);


\echo 3 (a)
(SELECT q.coefficient + p.coefficient as coefficient, q.degree
FROM P p, Q q
WHERE p.degree = q.degree)
UNION
(SELECT *
FROM (SELECT * FROM P p where p.degree not in (SELECT q.degree from Q q)) _)
UNION
(SELECT *
FROM (SELECT * FROM Q q where q.degree not in (SELECT p.degree from P p)) _);

\echo 3 (b)
SELECT SUM(q.coefficient * p.coefficient) as coefficient, (p.degree + q.degree) as degree
FROM P p, Q q
GROUP BY (p.degree + q.degree);


DROP TABLE P CASCADE;
DROP TABLE Q CASCADE;


\echo
--Problem 4
\echo PROBLEM 4 '\n'----------------------------------------------
CREATE TABLE Point (pid INTEGER PRIMARY KEY, x FLOAT, y FLOAT);

INSERT INTO POINT values(1,0,0);
INSERT INTO POINT values(2,0,1);
INSERT INTO POINT values(3,1,0);
INSERT INTO POINT values(4,0,2);
INSERT INTO POINT values(5,1,1);
INSERT INTO POINT values(6,2,2);

\echo 4 (a)
SELECT p1, p2
FROM 
	(SELECT p1.pid as p1, p2.pid as p2, |/ ((p1.x - p2.x) ^ 2 + (p1.y - p2.y) ^ 2) as dist
	FROM Point p1, Point p2
	WHERE not p1.pid = p2.pid) calc
WHERE calc.dist <= ALL (SELECT |/ ((p1.x - p2.x) ^ 2 + (p1.y - p2.y) ^ 2)
			FROM Point p1, Point p2
			WHERE not p1.pid = p2.pid);

\echo 4 (b)
SELECT p1.pid, p2.pid, p3.pid
FROM Point p1, Point p2, Point p3
WHERE 	(NOT ((p1.x - p2.x) = 0)
	AND NOT ((p2.x - p3.x) = 0)
	AND NOT ((p1.x - p3.x) = 0)
	AND (((p1.y - p2.y) / (p1.x - p2.x)) = ((p2.y - p3.y) / (p2.x - p3.x)))
	AND (((p2.y - p3.y) / (p2.x - p3.x)) = ((p1.y - p3.y) / (p1.x - p3.x))))
	OR
	((p1.x = p2.x) AND (p1.x = p3.x) AND (NOT p1.y = p2.y) AND (NOT p3.y = p2.y) AND (NOT p3.y = p1.y));


DROP TABLE Point CASCADE;

\echo
--Problem 5
\echo PROBLEM 5 '\n'----------------------------------------------


CREATE TABLE student(Sid Integer, Sname VARCHAR(15), PRIMARY KEY(Sid));
CREATE TABLE major(Sid Integer REFERENCES student(Sid), Major VARCHAR(15), PRIMARY KEY(Sid, Major));
CREATE TABLE book(BookNo Integer, Title VARCHAR(30), Price Integer, PRIMARY KEY(BookNo));
CREATE TABLE cites(BookNo Integer REFERENCES book(BookNo), CitedBookNo Integer REFERENCES book(BookNo), PRIMARY KEY(BookNo, CitedBookNo));
CREATE TABLE buys(Sid Integer REFERENCES student(Sid), BookNo Integer REFERENCES book(BookNo), PRIMARY KEY(Sid, BookNo));

-- Data for the student relation.
INSERT INTO student VALUES(1001,'Jean');
INSERT INTO student VALUES(1002,'Maria');
INSERT INTO student VALUES(1003,'Anna');
INSERT INTO student VALUES(1004,'Chin');
INSERT INTO student VALUES(1005,'John');
INSERT INTO student VALUES(1006,'Ryan');
INSERT INTO student VALUES(1007,'Catherine');
INSERT INTO student VALUES(1008,'Emma');
INSERT INTO student VALUES(1009,'Jan');
INSERT INTO student VALUES(1010,'Linda');
INSERT INTO student VALUES(1011,'Nick');
INSERT INTO student VALUES(1012,'Eric');
INSERT INTO student VALUES(1013,'Lisa');
INSERT INTO student VALUES(1014,'Filip');
INSERT INTO student VALUES(1015,'Dirk');
INSERT INTO student VALUES(1016,'Mary');
INSERT INTO student VALUES(1017,'Ellen');
INSERT INTO student VALUES(1020,'Ahmed');
INSERT INTO student VALUES(1021,'Vince');
INSERT INTO student VALUES(1022,'Joeri');

-- Data for the book relation.

INSERT INTO book VALUES(2001,'Databases',40);
INSERT INTO book VALUES(2002,'OperatingSystems',25);
INSERT INTO book VALUES(2003,'Networks',20);
INSERT INTO book VALUES(2004,'AI',45);
INSERT INTO book VALUES(2005,'DiscreteMathematics',20);
INSERT INTO book VALUES(2006,'SQL',25);
INSERT INTO book VALUES(2007,'ProgrammingLanguages',15);
INSERT INTO book VALUES(2008,'DataScience',50);
INSERT INTO book VALUES(2009,'Calculus',10);
INSERT INTO book VALUES(2010,'Philosophy',25);
INSERT INTO book VALUES(2012,'Geometry',80);
INSERT INTO book VALUES(2013,'RealAnalysis',35);
INSERT INTO book VALUES(2011,'Anthropology',50);
INSERT INTO book VALUES(2014,'Topology',70);

-- Data for the buys relation.

INSERT INTO buys VALUES(1001,2002);
INSERT INTO buys VALUES(1001,2007);
INSERT INTO buys VALUES(1001,2009);
INSERT INTO buys VALUES(1001,2011);
INSERT INTO buys VALUES(1001,2013);
INSERT INTO buys VALUES(1002,2001);
INSERT INTO buys VALUES(1002,2002);
INSERT INTO buys VALUES(1002,2007);
INSERT INTO buys VALUES(1002,2011);
INSERT INTO buys VALUES(1002,2012);
INSERT INTO buys VALUES(1002,2013);
INSERT INTO buys VALUES(1003,2002);
INSERT INTO buys VALUES(1003,2007);
INSERT INTO buys VALUES(1003,2011);
INSERT INTO buys VALUES(1003,2012);
INSERT INTO buys VALUES(1003,2013);
INSERT INTO buys VALUES(1004,2006);
INSERT INTO buys VALUES(1004,2007);
INSERT INTO buys VALUES(1004,2008);
INSERT INTO buys VALUES(1004,2011);
INSERT INTO buys VALUES(1004,2012);
INSERT INTO buys VALUES(1004,2013);
INSERT INTO buys VALUES(1005,2007);
INSERT INTO buys VALUES(1005,2011);
INSERT INTO buys VALUES(1005,2012);
INSERT INTO buys VALUES(1005,2013);
INSERT INTO buys VALUES(1006,2006);
INSERT INTO buys VALUES(1006,2007);
INSERT INTO buys VALUES(1006,2008);
INSERT INTO buys VALUES(1006,2011);
INSERT INTO buys VALUES(1006,2012);
INSERT INTO buys VALUES(1006,2013);
INSERT INTO buys VALUES(1007,2001);
INSERT INTO buys VALUES(1007,2002);
INSERT INTO buys VALUES(1007,2003);
INSERT INTO buys VALUES(1007,2007);
INSERT INTO buys VALUES(1007,2008);
INSERT INTO buys VALUES(1007,2009);
INSERT INTO buys VALUES(1007,2010);
INSERT INTO buys VALUES(1007,2011);
INSERT INTO buys VALUES(1007,2012);
INSERT INTO buys VALUES(1007,2013);
INSERT INTO buys VALUES(1008,2007);
INSERT INTO buys VALUES(1008,2011);
INSERT INTO buys VALUES(1008,2012);
INSERT INTO buys VALUES(1008,2013);
INSERT INTO buys VALUES(1009,2001);
INSERT INTO buys VALUES(1009,2002);
INSERT INTO buys VALUES(1009,2011);
INSERT INTO buys VALUES(1009,2012);
INSERT INTO buys VALUES(1009,2013);
INSERT INTO buys VALUES(1010,2001);
INSERT INTO buys VALUES(1010,2002);
INSERT INTO buys VALUES(1010,2003);
INSERT INTO buys VALUES(1010,2011);
INSERT INTO buys VALUES(1010,2012);
INSERT INTO buys VALUES(1010,2013);
INSERT INTO buys VALUES(1011,2002);
INSERT INTO buys VALUES(1011,2011);
INSERT INTO buys VALUES(1011,2012);
INSERT INTO buys VALUES(1012,2011);
INSERT INTO buys VALUES(1012,2012);
INSERT INTO buys VALUES(1013,2001);
INSERT INTO buys VALUES(1013,2011);
INSERT INTO buys VALUES(1013,2012);
INSERT INTO buys VALUES(1014,2008);
INSERT INTO buys VALUES(1014,2011);
INSERT INTO buys VALUES(1014,2012);
INSERT INTO buys VALUES(1017,2001);
INSERT INTO buys VALUES(1017,2002);
INSERT INTO buys VALUES(1017,2003);
INSERT INTO buys VALUES(1017,2008);
INSERT INTO buys VALUES(1017,2012);
INSERT INTO buys VALUES(1020,2001);
INSERT INTO buys VALUES(1020,2012);
INSERT INTO buys VALUES(1022,2014);

-- Data for the cites relation.
INSERT INTO cites VALUES(2012,2001);
INSERT INTO cites VALUES(2008,2011);
INSERT INTO cites VALUES(2008,2012);
INSERT INTO cites VALUES(2001,2002);
INSERT INTO cites VALUES(2001,2007);
INSERT INTO cites VALUES(2002,2003);
INSERT INTO cites VALUES(2003,2001);
INSERT INTO cites VALUES(2003,2004);
INSERT INTO cites VALUES(2003,2002);

-- Data for the cites relation.

INSERT INTO major VALUES(1001,'Math');
INSERT INTO major VALUES(1001,'Physics');
INSERT INTO major VALUES(1002,'CS');
INSERT INTO major VALUES(1002,'Math');
INSERT INTO major VALUES(1003,'Math');
INSERT INTO major VALUES(1004,'CS');
INSERT INTO major VALUES(1006,'CS');
INSERT INTO major VALUES(1007,'CS');
INSERT INTO major VALUES(1007,'Physics');
INSERT INTO major VALUES(1008,'Physics');
INSERT INTO major VALUES(1009,'Biology');
INSERT INTO major VALUES(1010,'Biology');
INSERT INTO major VALUES(1011,'CS');
INSERT INTO major VALUES(1011,'Math');
INSERT INTO major VALUES(1012,'CS');
INSERT INTO major VALUES(1013,'CS');
INSERT INTO major VALUES(1013,'Psychology');
INSERT INTO major VALUES(1014,'Theater');
INSERT INTO major VALUES(1017,'Anthropology');
INSERT INTO major VALUES(1022,'CS');

-- 5 (a)
\echo 5 (a)
CREATE FUNCTION booksBoughtbyStudent(sid INTEGER, out BookNo INTEGER, out title VARCHAR(30), out price INTEGER)
RETURNS SETOF record
as
$$
	SELECT bo.BookNo, bo.Title, bo.Price
	FROM buys b, book bo
	WHERE b.Sid = $1 AND bo.BookNo = b.BookNo
$$ LANGUAGE SQL;

-- 5 (b)
\echo 5 (b)
SELECT *
FROM booksBoughtByStudent(1001);

SELECT *
FROM booksBoughtByStudent(1015);


-- 5 (c) (i)
\echo 5 (c) (i)
SELECT s.sid, s.sname
FROM student s, booksBoughtByStudent(s.Sid) b
WHERE b.Price < 50
	AND NOT EXISTS(
		SELECT *
		FROM student s1, booksBoughtByStudent(s1.Sid) b1
		WHERE s.sid = s1.sid AND b1.Price < 50 AND (NOT b.bookno = b1.bookno));

-- 5 (c) (ii)
\echo 5 (c) (ii)
SELECT s.sid
FROM student s
WHERE 	(s.sid in (select m.sid from major m where m.major = 'CS'))
	AND
	NOT EXISTS(

	(SELECT BookNo FROM booksBoughtByStudent(s.Sid))
	
	INTERSECT

	(SELECT DISTINCT b.BookNo
	FROM major m, booksBoughtByStudent(m.Sid) b
	WHERE m.major = 'Math'));


-- 5 (c) (iii)
\echo 5 (c) (iii)
SELECT s1.sid, s2.sid
FROM student s1, student s2
WHERE 	NOT s1.sid = s2.sid
	AND
	NOT EXISTS((select bookno from booksBoughtByStudent(s1.sid)) EXCEPT (select bookno from booksBoughtByStudent(s2.sid)))
	AND
	NOT EXISTS((select bookno from booksBoughtByStudent(s2.sid)) EXCEPT (select bookno from booksBoughtByStudent(s1.sid)));


\echo
--Problem 6
\echo PROBLEM 6 '\n'----------------------------------------------

-- 6 (a)
\echo 6 (a)
CREATE FUNCTION studentWhoBoughtBook(bookno INTEGER, out sid INTEGER, out sname VARCHAR(15))
RETURNS SETOF record
as
$$
	SELECT s.sid, s.sname
	FROM buys b, student s
	WHERE s.sid = b.sid and b.bookno = $1
$$ LANGUAGE SQL;

-- 6 (b)
\echo 6 (b)
SELECT *
FROM studentWhoBoughtBook(2001);

SELECT *
FROM studentWhoBoughtBook(2010);

-- 6 (c)
\echo 6 (c)
CREATE FUNCTION studentBoughtBookGreaterThan(sid INTEGER, price INTEGER, out res boolean)
as
$$
	SELECT EXISTS(	SELECT *
		FROM buys bu, book bo
		WHERE bo.bookno = bu.bookno AND bo.price > $2 AND $1 = bu.sid)
$$ LANGUAGE SQL;


CREATE FUNCTION booksBoughtByBoth(sid1 INTEGER, sid2 INTEGER, out bookno INTEGER)
RETURNS SETOF INTEGER
as
$$	SELECT Bookno FROM
		((SELECT Bookno FROM booksBoughtByStudent(sid1))
		INTERSECT
		(SELECT Bookno FROM booksBoughtByStudent(sid2))) _
$$ LANGUAGE SQL;


SELECT DISTINCT b.bookno
FROM 
	(SELECT m.sid
	FROM major m
	WHERE m.major = 'CS' and studentBoughtBookGreaterThan(m.sid,30)) s1,
	
	(SELECT m.sid
	FROM major m, studentBoughtBookGreaterThan(m.sid,30)
	WHERE m.major = 'CS'and studentBoughtBookGreaterThan(m.sid,30)) s2,
	
	booksBoughtByBoth(s1.sid, s2.sid) b
WHERE NOT s1.sid = s2.sid
ORDER BY b.bookno;


\echo
--Problem 7
\echo PROBLEM 7 '\n'----------------------------------------------
\echo 7 (a)
-- 7 (a)
CREATE FUNCTION numberOfBooksBoughtbyStudent(sid INTEGER, out numBooks INTEGER)
as
$$
	SELECT CAST (COUNT(b.bookno) as INTEGER)
	FROM booksBoughtbyStudent(sid) b
$$ LANGUAGE SQL;

\echo 7 (b) (i)
-- 7 (b) (i)

SELECT m.sid, nb.numBooks as numberofbooksboughtbystudent
FROM major m, numberofbooksboughtbystudent(m.sid) nb
WHERE m.major = 'CS' AND nb.numBooks > 2;

-- 7 (b) (ii)
\echo 7 (b) (ii)

SELECT s.sid
FROM student s, numberofbooksboughtbystudent(s.sid) nb1
WHERE nb1.numbooks <= SOME(
			SELECT nb.numbooks
			FROM major m, numberofbooksboughtbystudent(m.sid) nb
			WHERE m.major = 'CS');

-- 7 (b) (iii)
\echo 7 (b) (iii)

SELECT s1.sid, s2.sid
FROM student s1, student s2
WHERE  numberofbooksboughtbystudent(s1.sid) = numberofbooksboughtbystudent(s2.sid)
	AND NOT s1.sid = s2.sid;


\echo
--Problem 8
\echo PROBLEM 8 '\n'----------------------------------------------
\echo 8 (a)
-- 8 (a)
CREATE FUNCTION costOfBooksBoughtbyStudent(sid INTEGER, out cost INTEGER)
as
$$
	SELECT CASE WHEN COUNT(*) > 0 THEN CAST (SUM(b.price) as INTEGER) ELSE 0 END
	FROM booksBoughtbyStudent(sid) b
$$ LANGUAGE SQL;

SELECT s1.sid, numberofbooksboughtbystudent(s1.sid)
FROM student s1
WHERE  costofbooksboughtbystudent(s1.sid) < 300;


-- 8 (b)
\echo 8 (b)

CREATE FUNCTION numberCited(bookno INTEGER, out numCited INTEGER)
as
$$
	SELECT CAST (Count(c.citedbookno) AS INTEGER)
	FROM cites c
	WHERE $1 = c.bookno
$$ LANGUAGE SQL;

CREATE FUNCTION numberCitedBy(bookno INTEGER, out numCitedBy INTEGER)
as
$$
	SELECT CAST (Count(bookno) as INTEGER)
	FROM cites c
	WHERE $1 = c.citedbookno
$$ LANGUAGE SQL;


SELECT bookno
FROM book
WHERE numberCited(bookno) >= 2 AND numberCitedBy(bookno) < 4;

-- 8 (c)
\echo 8 (c)
SELECT bookno, title
FROM book b
WHERE b.price = (SELECT MIN(price) FROM book);


-- 8 (d)
\echo 8 (d)
SELECT s.sid, b.bookno
FROM student s, book b
WHERE 	b.price = (SELECT MIN(b1.price)
			FROM book b1
			WHERE 	b1.bookno in
				(SELECT bookno FROM booksBoughtByStudent(s.Sid)))
	AND b.bookno in (SELECT bookno from booksBoughtByStudent(s.sid))
ORDER BY s.sid;

-- 8 (e)
\echo 8 (e)
SELECT m.major
FROM
	(SELECT m.major, SUM(b.price) as cost
	FROM major m, booksBoughtByStudent(m.sid) b
	GROUP BY m.major) m
WHERE m.cost =
	(SELECT MAX(p.cost)
	FROM
		(SELECT SUM(b.price) as cost
		FROM major m, booksBoughtByStudent(m.sid) b
		GROUP BY m.major) p);


-- 8 (f)
\echo 8 (f)
SELECT b.bookno
FROM

	(SELECT COUNT(*) as num
	FROM major m
	WHERE m.major = 'Biology') s,


	(SELECT b.bookno, Count(b.bookno) as num
	FROM major m, booksBoughtByStudent(m.sid) b
	WHERE	m.major = 'Biology'
	GROUP BY b.bookno) b

WHERE b.num = s.num;

-- 8 (g)
\echo 8 (g)
CREATE FUNCTION NumBoughtByBothHelper(sone INTEGER, stwo INTEGER, out num INTEGER)
as
$$
	SELECT CAST(COUNT(*) as INTEGER) as num
	FROM booksboughtbyBoth(sone, stwo)
$$ LANGUAGE SQL;


CREATE FUNCTION NumBoughtByBoth(out sone INTEGER, out stwo INTEGER, out num INTEGER)
RETURNS SETOF record
as
$$
	SELECT s1.sid as sone, s2.sid as stwo, NumBoughtByBothHelper(s1.sid, s2.sid) as num
	FROM student s1, student s2
	WHERE NOT s1.sid = s2.sid
	GROUP BY s1.sid, s2.sid
$$ LANGUAGE SQL;


SELECT s.sone, s.stwo
FROM NumBoughtByBoth() s
WHERE s.num = 1;

-- 8 (h)		
\echo 8 (h)
CREATE FUNCTION Allbutk(k INTEGER, out sone INTEGER, out stwo INTEGER)
RETURNS SETOF record
as
$$
	SELECT s1.sid, s2.sid
	FROM student s1, student s2,  NumBoughtByBoth() n
	WHERE 	s1.sid = n.sone
	AND s2.sid = n.stwo
	AND numberofbooksboughtbystudent(s2.sid) - n.num = k; 
$$ LANGUAGE SQL;

SELECT *
FROM Allbutk(1);




DROP FUNCTION NumBoughtByBothHelper(sone INTEGER, stwo INTEGER, out num INTEGER);
DROP FUNCTION NumBoughtByBoth(out sone INTEGER, out stwo INTEGER, out num INTEGER);
DROP FUNCTION Allbutk(k INTEGER, out sone INTEGER, out stwo INTEGER);
DROP FUNCTION numberCitedBy(bookno INTEGER, out numCitedBy INTEGER);
DROP FUNCTION numberCited(bookno INTEGER, out numCited INTEGER);
DROP FUNCTION costOfBooksBoughtbyStudent(sid INTEGER, out cost INTEGER);
DROP FUNCTION numberOfBooksBoughtbyStudent(sid INTEGER, out numBooks INTEGER);
DROP FUNCTION booksBoughtByBoth(sid1 INTEGER, sid2 INTEGER, out bookno INTEGER);
DROP FUNCTION studentBoughtBookGreaterThan(sid INTEGER, price INTEGER, out boolean);
DROP FUNCTION studentWhoBoughtBook(bookno INTEGER, out student sid, out sname VARCHAR(15));
DROP FUNCTION booksBoughtbyStudent(sid INTEGER, out bookno INTEGER);
DROP TABLE buys CASCADE;
DROP TABLE cites CASCADE;
DROP TABLE book CASCADE;
DROP TABLE major CASCADE;
DROP TABLE student CASCADE;
