-- Data for Assignment 4 

CREATE TABLE student(Sid Integer, Sname VARCHAR(15), PRIMARY KEY(Sid));
CREATE TABLE major(Sid Integer REFERENCES student(Sid), Major VARCHAR(15), PRIMARY KEY(Sid, Major));
CREATE TABLE book(BookNo Integer, Title VARCHAR(30), Price Integer, PRIMARY KEY(BookNo));
CREATE TABLE cites(BookNo Integer REFERENCES book(BookNo), CitedBookNo Integer REFERENCES book(BookNo), PRIMARY KEY(BookNo, CitedBookNo));
CREATE TABLE buys(Sid Integer REFERENCES student(Sid), BookNo Integer REFERENCES book(BookNo), PRIMARY KEY(Sid, BookNo));

-- student 

insert into student values(1001,'Jean');
insert into student values(1002,'Maria');
insert into student values(1003,'Anna');
insert into student values(1004,'Chin');
insert into student values(1005,'John');
insert into student values(1006,'Ryan');
insert into student values(1007,'Catherine');
insert into student values(1008,'Emma');
insert into student values(1009,'Jan');
insert into student values(1010,'Linda');
insert into student values(1011,'Nick');
insert into student values(1012,'Eric');
insert into student values(1013,'Lisa');
insert into student values(1014,'Filip');
insert into student values(1015,'Dirk');
insert into student values(1016,'Mary');
insert into student values(1017,'Ellen');
insert into student values(1020,'Ahmed');
insert into student values(1021,'Vince');
insert into student values(1022,'Joeri');
insert into student values(1023,'Chris');
insert into student values(1040,'Sofie');


-- book 

insert into book values(2001,'Databases',40);
insert into book values(2002,'OperatingSystems',25);
insert into book values(2003,'Networks',20);
insert into book values(2004,'AI',45);
insert into book values(2005,'DiscreteMathematics',20);
insert into book values(2006,'SQL',25);
insert into book values(2007,'ProgrammingLanguages',15);
insert into book values(2008,'DataScience',50);
insert into book values(2009,'Calculus',10);
insert into book values(2010,'Philosophy',25);
insert into book values(2012,'Geometry',80);
insert into book values(2013,'RealAnalysis',35);
insert into book values(2011,'Anthropology',50);
insert into book values(2014,'Topology',70);

-- cites 
insert into cites values(2012,2001);
insert into cites values(2008,2011);
insert into cites values(2008,2012);
insert into cites values(2001,2002);
insert into cites values(2001,2007);
insert into cites values(2002,2003);
insert into cites values(2003,2001);
insert into cites values(2003,2004);
insert into cites values(2003,2002);
insert into cites values(2010,2001);
insert into cites values(2010,2002);
insert into cites values(2010,2003);
insert into cites values(2010,2004);
insert into cites values(2010,2005);
insert into cites values(2010,2006);
insert into cites values(2010,2007);
insert into cites values(2010,2008);
insert into cites values(2010,2009);
insert into cites values(2010,2011);
insert into cites values(2010,2013);
insert into cites values(2010,2014);

-- buys
insert into buys values(1023,2012);
insert into buys values(1023,2014);
insert into buys values(1040,2002);
insert into buys values(1001,2002);
insert into buys values(1001,2007);
insert into buys values(1001,2009);
insert into buys values(1001,2011);
insert into buys values(1001,2013);
insert into buys values(1002,2001);
insert into buys values(1002,2002);
insert into buys values(1002,2007);
insert into buys values(1002,2011);
insert into buys values(1002,2012);
insert into buys values(1002,2013);
insert into buys values(1003,2002);
insert into buys values(1003,2007);
insert into buys values(1003,2011);
insert into buys values(1003,2012);
insert into buys values(1003,2013);
insert into buys values(1004,2006);
insert into buys values(1004,2007);
insert into buys values(1004,2008);
insert into buys values(1004,2011);
insert into buys values(1004,2012);
insert into buys values(1004,2013);
insert into buys values(1005,2007);
insert into buys values(1005,2011);
insert into buys values(1005,2012);
insert into buys values(1005,2013);
insert into buys values(1006,2006);
insert into buys values(1006,2007);
insert into buys values(1006,2008);
insert into buys values(1006,2011);
insert into buys values(1006,2012);
insert into buys values(1006,2013);
insert into buys values(1007,2001);
insert into buys values(1007,2002);
insert into buys values(1007,2003);
insert into buys values(1007,2007);
insert into buys values(1007,2008);
insert into buys values(1007,2009);
insert into buys values(1007,2010);
insert into buys values(1007,2011);
insert into buys values(1007,2012);
insert into buys values(1007,2013);
insert into buys values(1008,2007);
insert into buys values(1008,2011);
insert into buys values(1008,2012);
insert into buys values(1008,2013);
insert into buys values(1009,2001);
insert into buys values(1009,2002);
insert into buys values(1009,2011);
insert into buys values(1009,2012);
insert into buys values(1009,2013);
insert into buys values(1010,2001);
insert into buys values(1010,2002);
insert into buys values(1010,2003);
insert into buys values(1010,2011);
insert into buys values(1010,2012);
insert into buys values(1010,2013);
insert into buys values(1011,2002);
insert into buys values(1011,2011);
insert into buys values(1011,2012);
insert into buys values(1012,2011);
insert into buys values(1012,2012);
insert into buys values(1013,2001);
insert into buys values(1013,2011);
insert into buys values(1013,2012);
insert into buys values(1014,2008);
insert into buys values(1014,2011);
insert into buys values(1014,2012);
insert into buys values(1017,2001);
insert into buys values(1017,2002);
insert into buys values(1017,2003);
insert into buys values(1017,2008);
insert into buys values(1017,2012);
insert into buys values(1020,2001);
insert into buys values(1020,2012);
insert into buys values(1022,2014);

-- major
insert into major values(1001,'Math');
insert into major values(1001,'Physics');
insert into major values(1002,'CS');
insert into major values(1002,'Math');
insert into major values(1003,'Math');
insert into major values(1004,'CS');
insert into major values(1006,'CS');
insert into major values(1007,'CS');
insert into major values(1007,'Physics');
insert into major values(1008,'Physics');
insert into major values(1009,'Biology');
insert into major values(1010,'Biology');
insert into major values(1011,'CS');
insert into major values(1011,'Math');
insert into major values(1012,'CS');
insert into major values(1013,'CS');
insert into major values(1013,'Psychology');
insert into major values(1014,'Theater');
insert into major values(1017,'Anthropology');
insert into major values(1022,'CS');
insert into major values(1015,'Chemistry');



-- 1
\echo
\echo Problem 1
WITH
E1 as (SELECT BookNo FROM Book WHERE price < 50),
E2 as (SELECT * FROM CITES NATURAL JOIN E1)
(SELECT DISTINCT citedbookno FROM E2 ORDER BY citedbookno);


-- 2
\echo
\echo Problem 2
WITH
E1 as (Select m1.sid FROM major m1, major m2 WHERE m1.sid = m2.sid AND m1.major = 'Math' AND m2.major = 'CS'),
E2 as (Select * FROM buys NATURAL JOIN E1),
E3 as (Select DISTINCT bookno, title FROM book NATURAL JOIN E2)
(SELECT * FROM e3); 

-- 3
\echo
\echo Problem 3
WITH
E1 as (SELECT BookNo FROM Book WHERE price < 50),
E2 as (SELECT * FROM CITES NATURAL JOIN E1),
E3 as (SELECT DISTINCT a.citedbookno as bookno FROM e2 a, e2 b WHERE a.citedbookno = b.citedbookno AND a.bookno <> b.bookno),
E4 as (SELECT * FROM E3 NATURAL JOIN buys)
(SELECT sid, bookno FROM E4 ORDER BY sid);

-- 4
\echo
\echo Problem 4
WITH
E1 as (SELECT BookNo FROM Book WHERE price > 50),
E2 as ((SELECT *
	FROM (Select sid from Student s1) s, E1 d)
 	EXCEPT
	(Select * from buys))
((SELECT DISTINCT sid from buys) EXCEPT (SELECT DISTINCT sid from E2));



--5
\echo
\echo Problem 5
WITH
E1 as (SELECT * FROM major WHERE major = 'CS'),
E2 as (SELECT DISTINCT bookno FROM E1 natural join buys),
E3 as (SELECT * FROM E2 natural join book),
E4 as (SELECT * FROM ((SELECT * FROM book) EXCEPT (SELECT * FROM E3)) b)
(SELECT bookno FROM E4); 


--6
\echo
\echo Problem 6

WITH
E1 as (SELECT * FROM major WHERE major = 'CS'),
E2 as (SELECT DISTINCT sid FROM E1 natural join student),
E3 as (SELECT * FROM E2, (SELECT bookno FROM book) b),
E4 as (SELECT * FROM ((SELECT * FROM E3) EXCEPT (Select * from Buys)) b)
(SELECT DISTINCT bookno FROM E4 ORDER BY bookno);

--7
\echo
\echo Problem 7
WITH
E1 as (SELECT * FROM buys b, (Select bookno as citedbookno from book) c),
E2 as (SELECT * FROM (SELECT sid from student) s, cites)
(SELECT DISTINCT sid, citedbookno FROM ((SELECT * FROM E1) EXCEPT (SELECT * FROM E2)) q);


--8
\echo
\echo Problem 8
WITH
E1 as (SELECT * FROM buys b, (Select bookno as citedbookno from book) c),
E2 as (SELECT * FROM (SELECT sid from student) s, cites),
Ans7 as (SELECT DISTINCT sid, citedbookno FROM ((SELECT * FROM E1) EXCEPT (SELECT * FROM E2)) q),
E3 as (SELECT * FROM (SELECT sid from student) s, (SELECT bookno as citedbookno FROM book) b)
(SELECT * FROM ((SELECT * FROM E3) EXCEPT (SELECT * FROM Ans7)) q ORDER BY sid);



DROP TABLE buys CASCADE;
DROP TABLE cites CASCADE;
DROP TABLE book CASCADE;
DROP TABLE major CASCADE;
DROP TABLE student CASCADE;

