-----CREATED BY: JACK LANGSTON

------------PROBLEM 1--------------------------------------

CREATE TABLE Boat(Bid INTEGER, Bname VARCHAR(15), Color VARCHAR(15), PRIMARY KEY(Bid));
COPY Boat FROM '/postgres/DatabaseConcepts/a1/boat.txt';

CREATE TABLE Sailor(Sid INTEGER, Sname VARCHAR(20), Rating INTEGER, Age INTEGER, PRIMARY KEY(Sid));
COPY Sailor FROM '/postgres/DatabaseConcepts/a1/sailor.txt';

CREATE TABLE Reserves(Sid INTEGER REFERENCES Sailor(Sid), Bid Integer REFERENCES Boat(Bid), Day VARCHAR(10));
COPY Reserves FROM '/postgres/DatabaseConcepts/a1/reserves.txt';

------------PROBLEM 2--------------------------------------

----------With foreign keys, cannot inset something that doesn't have a coresponding value in the
----------referenced table.
INSERT INTO Reserves VALUES (10,101,'Wednesday');

----------Additionally, we can't delete values in the referenced table that still have references
----------from the referencing table.
DELETE FROM Boat WHERE Bid = 101;

----------With primary keys, we can't insert an item that has a primary key that is already being used
----------in the table.

INSERT INTO Boat VALUES (101,'Jack','red');


------------PROBLEM 3--------------------------------------

\echo '\nPROBLEM 3 (a)'
SELECT Sid, rating FROM Sailor;

\echo '\nPROBLEM 3 (b)'
SELECT Bname FROM Boat WHERE Color = 'red';

\echo '\nPROBLEM 3 (c)'
SELECT Bname, Color FROM Boat;

\echo '\nPROBLEM 3 (d)'
SELECT Sname FROM Sailor WHERE Sid IN
	(SELECT Sid FROM Reserves WHERE Bid IN
		(SELECT Bid FROM Boat WHERE Color = 'red')
	);


\echo '\nPROBLEM 3 (e)'
SELECT Bname FROM Boat WHERE Bid IN
	(SELECT Bid FROM Reserves WHERE Sid IN
		(SELECT Sid FROM Sailor WHERE Age > 25)
	);

\echo '\nPROBLEM 3 (f)'
SELECT Sname FROM Sailor WHERE Sid IN
	(SELECT Sid FROM Reserves WHERE Bid IN
		(SELECT Bid FROM Boat WHERE Color != 'red' AND Color != 'green')
	);

\echo '\nPROBLEM 3 (g)'
SELECT Bname FROM Boat WHERE Bid IN
	(SELECT Bid FROM Reserves WHERE
	 	Sid IN (Select Sid FROM Reserves Where 
			Bid IN (SELECT Bid FROM Boat WHERE Color = 'blue'))
		AND
		Sid IN (Select Sid FROM Reserves Where
			Bid IN (SELECT Bid FROM Boat WHERE Color = 'green')));


\echo '\nPROBLEM 3 (h)'
SELECT Bid FROM Boat WHERE NOT Bid IN
	(SELECT Bid FROM Reserves);

\echo '\nPROBLEM 3 (i)'
SELECT Bname FROM Boat WHERE Bid IN
	(SELECT Bid FROM Reserves GROUP BY Bid HAVING COUNT(*) > 1);

\echo '\nPROBLEM 3 (j)'
SELECT Sname FROM Sailor WHERE Sid IN
	(SELECT Sid FROM Reserves GROUP BY Sid HAVING COUNT(*) = 1);


------------CLEAN UP--------------------------------------

DROP TABLE Reserves;
DROP TABLE Boat;
DROP TABLE Sailor;

