﻿--Assignment 7
--Completed by: Jack Langston

--Problem 1

\echo Problem 1

CREATE TABLE Graph(source Integer, target Integer);
CREATE TABLE APTable(node Integer);

INSERT INTO Graph VALUES (1, 2), (2, 1), (1,3), (3,1),(2,3),(3,2),(2,4),(4,2),(2,5),(5,2),(4,5),(5,4);

--Returns a table containing the nodes in Graph
CREATE OR REPLACE FUNCTION GET_NODES ()
RETURNS TABLE(node Integer) AS $$
BEGIN
	RETURN QUERY
	(select distinct source from graph)
	UNION
	(select distinct target from graph);
END;
$$ LANGUAGE plpgsql;



--Fills APTable with the articulation points in Graph
CREATE OR REPLACE FUNCTION AP ()
RETURNS SETOF Integer AS $$

DECLARE remnode Integer;
DECLARE tempNode Integer;
DECLARE edge record;

BEGIN

CREATE TABLE closed(node Integer);
CREATE TABLE q(node Integer);
DELETE FROM APTable;

for remnode in SELECT * FROM GET_NODES()
LOOP

	--Clear all temps
	DELETE FROM closed;
	DELETE FROM q;


	--Select some point that is not the removed node
	SELECT INTO tempNode source from graph where source <> remnode;
	INSERT INTO q VALUES (tempNode);


	--Create closure
	while (select count(*) from q) > 0
	LOOP

		--pop from q
		SELECT INTO tempNode node from q;
		DELETE FROM q where node = tempNode;

		--add to closed set
		INSERT INTO closed VALUES (tempNode);

		--traverse all of the edges that dont go to the removed node
		for edge in SELECT * FROM graph where source = tempNode and target <> remnode
		LOOP

		--if one of the edges doesnt already exist in the closed set, add it to the q to explore later
		IF edge.target not in (SELECT * FROM closed) THEN
		INSERT INTO q VALUES (edge.target);
		END IF;

		END LOOP;

	END LOOP;


	--if the closed set doesnt contain all of the nodes (minus the one we removed), then the removed node is an articulation point
	IF (SELECT Count(*) from closed) < (SELECT Count(*) from GET_NODES()) - 1 THEN
		INSERT INTO APTable VALUES (remnode);
	END IF;

END LOOP;

DROP TABLE closed;
DROP TABLE q;

RETURN QUERY SELECT * FROM APTable;


END;
$$ LANGUAGE plpgsql;
SELECT  * FROM AP();


--Problem 2
\echo '\n\n\n'
\echo Problem 2

--input
CREATE TABLE Parent_Child(pid Integer, cid Integer);
INSERT INTO Parent_Child VALUES (1,2),(1,3), (2,4), (3,6), (6,8), (1,5), (5,9);

--intermediate table that stores nodes mapped to their generation
CREATE TABLE GENS(id Integer, gen Integer);


--gets the root of the tree
CREATE OR REPLACE FUNCTION GET_ROOT ()
RETURNS Setof Integer AS $$
BEGIN
	RETURN QUERY
	SELECT pid
	FROM Parent_Child
	WHERE pid not in (SELECT cid from Parent_Child);
END;
$$ LANGUAGE plpgsql;



--fills the generation table recursively
CREATE OR REPLACE FUNCTION FILL_GEN(parent Integer, depth Integer)
RETURNS VOID AS $$
DECLARE edge record;
BEGIN
	FOR edge in SELECT * from Parent_Child where pid = parent
	LOOP
		INSERT INTO GENS VALUES (edge.cid, depth);
		PERFORM FILL_GEN(edge.cid, depth + 1);
	END LOOP;
END
$$ LANGUAGE plpgsql;



--generates the solution for problem 2
CREATE FUNCTION SAME_GEN ()
RETURNS TABLE(node1 Integer, node2 Integer) AS $$

DECLARE root Integer;

BEGIN

-- set root
SELECT INTO root GET_ROOT();

--reset gens
DELETE FROM GENS;
INSERT INTO GENS VALUES (root, 0);

--initiate the recursive fill of gen
PERFORM FILL_GEN(root, 1);

RETURN QUERY
SELECT g1.id, g2.id
FROM GENS g1, GENS g2
WHERE g1.gen = g2.gen AND g1.id <> g2.id;

END
$$ LANGUAGE plpgsql;


SELECT * FROM SAME_GEN();


--Problem 3
\echo '\n\n\n'
\echo Problem 3
CREATE TABLE powerset(subset Integer[]);
CREATE TABLE powerset_input(elem Integer);
INSERT INTO powerset_input VALUES (1), (2), (3), (4), (5);

CREATE OR REPLACE FUNCTION FILL_POWERSET(cur Integer[], orig Integer[], nextIndex Integer)
RETURNS VOID AS $$
DECLARE size Integer;
DECLARE i Integer;
BEGIN

	--insert the current
	INSERT INTO powerset Values (cur);
	

	--recur
	size := cardinality(orig);
	i := nextIndex;
	while i <= size
	LOOP
		PERFORM FILL_POWERSET(cur || orig[i], orig, i + 1);
		i := i + 1;
	END LOOP;
END
$$ LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION POWERSET()
RETURNS TABLE(subset Integer[]) AS $$
BEGIN
	PERFORM FILL_POWERSET(cast(Array[] as Integer[]), ARRAY(SELECT * FROM powerset_input), 1);

	RETURN QUERY
	SELECT * FROM powerset;
END
$$ LANGUAGE PLPGSQL;

SELECT POWERSET();


--Problem 4
\echo '\n\n\n'
\echo Problem 4

CREATE TABLE WeightedGraph (source Integer, target Integer, cost Integer);
INSERT INTO WeightedGraph VALUES (1,2,5), (2,1,5), (1,3,3), (3,1,3),(2,3,2),(3,2,2),(2,5,2),(5,2,2),(3,5,4),(5,3,4),(2,4,8),(4,2,8); 

CREATE TABLE SpanningTree(source Integer, target Integer);


--Fills SpanningTree with the articulation points in Graph
CREATE OR REPLACE FUNCTION FILL_SpanningTree ()
RETURNS TABLE(source Integer, target Integer) AS $$

DECLARE remnode Integer;
DECLARE tedge record;
DECLARE edge record;

BEGIN

CREATE TABLE q(source Integer, target Integer, cost Integer);
CREAte TABLE closed(node Integer);

--Clear all temps
DELETE FROM SpanningTree;


--Select the smallest edge
SELECT INTO edge * from weightedgraph w where w.cost = (select min(cost) from weightedgraph);

--add all edges connected to the node with the smallest edge to the q
for tedge in (SELECT w.* FROM weightedGraph w where w.source = edge.source)
LOOP
	INSERT INTO q VALUES (tedge.source, tedge.target, tedge.cost);
END LOOP;

--Traverse graph
while (select count(*) from q) > 0
LOOP

	--pop from q
	SELECT INTO edge * from q where cost = (select min(cost) from q);
	DELETE FROM q where q.target = edge.target; --note: we want to delete all edges that share the same target

	--add to closed set
	INSERT INTO spanningtree VALUES (edge.source, edge.target), (edge.target, edge.source);
	INSERT INTO closed VALUES (edge.source), (edge.target);

	--examine all the edge stemming from target
	for tedge in (SELECT w.* FROM weightedGraph w where w.source = edge.target)
	LOOP

		--if one of the edges connects to a node we havent added to the tree yet, add it to the q to explore later
		IF tedge.target not in (SELECT * FROM closed) THEN
			INSERT INTO q VALUES (tedge.source, tedge.target, tedge.cost);
		END IF;

	END LOOP;

END LOOP;


DROP TABLE closed;
DROP TABLE q;

RETURN QUERY SELECT * FROM SpanningTree;

END;
$$ LANGUAGE plpgsql;
SELECT * FROM FILL_SpanningTree();


--Problem 5
\echo '\n\n\n'
\echo Problem 5
\echo 'Ran out of time'


DROP FUNCTION FILL_SPANNINGTREE();
DROP FUNCTION POWERSET();
DROP FUNCTION FILL_POWERSET(cur Integer[], todo Integer[], nextIndex Integer);
DROP FUNCTION GET_ROOT();
DROP FUNCTION SAME_GEN();
DROP FUNCTION GET_NODES();
DROP FUNCTION FILL_GEN(parent INTEGER, depth INTEGER);
DROP FUNCTION AP();
DROP TABLE Gens;
DROP TABLE Parent_Child;
DROP TABLE Graph;
DROP TABLE APTABLE;
DROP TABLE powerset;
DROP TABLE powerset_input;
DROP TABLE WeightedGraph;
DROP TABLE SpanningTree;