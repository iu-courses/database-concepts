-- Created by Jack Langston

CREATE TABLE student(Sid Integer, Sname VARCHAR(15), PRIMARY KEY(Sid));
CREATE TABLE major(Sid Integer REFERENCES student(Sid), Major VARCHAR(15), PRIMARY KEY(Sid, Major));
CREATE TABLE book(BookNo Integer, Title VARCHAR(30), Price Integer, PRIMARY KEY(BookNo));
CREATE TABLE cites(BookNo Integer REFERENCES book(BookNo), CitedBookNo Integer REFERENCES book(BookNo), PRIMARY KEY(BookNo, CitedBookNo));
CREATE TABLE buys(Sid Integer REFERENCES student(Sid), BookNo Integer REFERENCES book(BookNo), PRIMARY KEY(Sid, BookNo));

-- Data for the student relation.
INSERT INTO student VALUES(1001,'Jean');
INSERT INTO student VALUES(1002,'Maria');
INSERT INTO student VALUES(1003,'Anna');
INSERT INTO student VALUES(1004,'Chin');
INSERT INTO student VALUES(1005,'John');
INSERT INTO student VALUES(1006,'Ryan');
INSERT INTO student VALUES(1007,'Catherine');
INSERT INTO student VALUES(1008,'Emma');
INSERT INTO student VALUES(1009,'Jan');
INSERT INTO student VALUES(1010,'Linda');
INSERT INTO student VALUES(1011,'Nick');
INSERT INTO student VALUES(1012,'Eric');
INSERT INTO student VALUES(1013,'Lisa');
INSERT INTO student VALUES(1014,'Filip');
INSERT INTO student VALUES(1015,'Dirk');
INSERT INTO student VALUES(1016,'Mary');
INSERT INTO student VALUES(1017,'Ellen');
INSERT INTO student VALUES(1020,'Ahmed');

-- Data for the book relation.

INSERT INTO book VALUES(2001,'Databases',40);
INSERT INTO book VALUES(2002,'OperatingSystems',25);
INSERT INTO book VALUES(2003,'Networks',20);
INSERT INTO book VALUES(2004,'AI',45);
INSERT INTO book VALUES(2005,'DiscreteMathematics',20);
INSERT INTO book VALUES(2006,'SQL',25);
INSERT INTO book VALUES(2007,'ProgrammingLanguages',15);
INSERT INTO book VALUES(2008,'DataScience',50);
INSERT INTO book VALUES(2009,'Calculus',10);
INSERT INTO book VALUES(2010,'Philosophy',25);
INSERT INTO book VALUES(2012,'Geometry',80);
INSERT INTO book VALUES(2013,'RealAnalysis',35);
INSERT INTO book VALUES(2011,'Anthropology',50);

-- Data for the buys relation.

INSERT INTO buys VALUES(1001,2002);
INSERT INTO buys VALUES(1001,2007);
INSERT INTO buys VALUES(1001,2009);
INSERT INTO buys VALUES(1001,2011);
INSERT INTO buys VALUES(1001,2013);
INSERT INTO buys VALUES(1002,2001);
INSERT INTO buys VALUES(1002,2002);
INSERT INTO buys VALUES(1002,2007);
INSERT INTO buys VALUES(1002,2011);
INSERT INTO buys VALUES(1002,2012);
INSERT INTO buys VALUES(1002,2013);
INSERT INTO buys VALUES(1003,2002);
INSERT INTO buys VALUES(1003,2007);
INSERT INTO buys VALUES(1003,2011);
INSERT INTO buys VALUES(1003,2012);
INSERT INTO buys VALUES(1003,2013);
INSERT INTO buys VALUES(1004,2006);
INSERT INTO buys VALUES(1004,2007);
INSERT INTO buys VALUES(1004,2008);
INSERT INTO buys VALUES(1004,2011);
INSERT INTO buys VALUES(1004,2012);
INSERT INTO buys VALUES(1004,2013);
INSERT INTO buys VALUES(1005,2007);
INSERT INTO buys VALUES(1005,2011);
INSERT INTO buys VALUES(1005,2012);
INSERT INTO buys VALUES(1005,2013);
INSERT INTO buys VALUES(1006,2006);
INSERT INTO buys VALUES(1006,2007);
INSERT INTO buys VALUES(1006,2008);
INSERT INTO buys VALUES(1006,2011);
INSERT INTO buys VALUES(1006,2012);
INSERT INTO buys VALUES(1006,2013);
INSERT INTO buys VALUES(1007,2001);
INSERT INTO buys VALUES(1007,2002);
INSERT INTO buys VALUES(1007,2003);
INSERT INTO buys VALUES(1007,2007);
INSERT INTO buys VALUES(1007,2008);
INSERT INTO buys VALUES(1007,2009);
INSERT INTO buys VALUES(1007,2010);
INSERT INTO buys VALUES(1007,2011);
INSERT INTO buys VALUES(1007,2012);
INSERT INTO buys VALUES(1007,2013);
INSERT INTO buys VALUES(1008,2007);
INSERT INTO buys VALUES(1008,2011);
INSERT INTO buys VALUES(1008,2012);
INSERT INTO buys VALUES(1008,2013);
INSERT INTO buys VALUES(1009,2001);
INSERT INTO buys VALUES(1009,2002);
INSERT INTO buys VALUES(1009,2011);
INSERT INTO buys VALUES(1009,2012);
INSERT INTO buys VALUES(1009,2013);
INSERT INTO buys VALUES(1010,2001);
INSERT INTO buys VALUES(1010,2002);
INSERT INTO buys VALUES(1010,2003);
INSERT INTO buys VALUES(1010,2011);
INSERT INTO buys VALUES(1010,2012);
INSERT INTO buys VALUES(1010,2013);
INSERT INTO buys VALUES(1011,2002);
INSERT INTO buys VALUES(1011,2011);
INSERT INTO buys VALUES(1011,2012);
INSERT INTO buys VALUES(1012,2011);
INSERT INTO buys VALUES(1012,2012);
INSERT INTO buys VALUES(1013,2001);
INSERT INTO buys VALUES(1013,2011);
INSERT INTO buys VALUES(1013,2012);
INSERT INTO buys VALUES(1014,2008);
INSERT INTO buys VALUES(1014,2011);
INSERT INTO buys VALUES(1014,2012);
INSERT INTO buys VALUES(1017,2001);
INSERT INTO buys VALUES(1017,2002);
INSERT INTO buys VALUES(1017,2003);
INSERT INTO buys VALUES(1017,2008);
INSERT INTO buys VALUES(1017,2012);
INSERT INTO buys VALUES(1020,2012);

-- Data for the cites relation.
INSERT INTO cites VALUES(2012,2001);
INSERT INTO cites VALUES(2008,2011);
INSERT INTO cites VALUES(2008,2012);
INSERT INTO cites VALUES(2001,2002);
INSERT INTO cites VALUES(2001,2007);
INSERT INTO cites VALUES(2002,2003);
INSERT INTO cites VALUES(2003,2001);
INSERT INTO cites VALUES(2003,2004);
INSERT INTO cites VALUES(2003,2002);

-- Data for the cites relation.

INSERT INTO major VALUES(1001,'Math');
INSERT INTO major VALUES(1001,'Physics');
INSERT INTO major VALUES(1002,'CS');
INSERT INTO major VALUES(1002,'Math');
INSERT INTO major VALUES(1003,'Math');
INSERT INTO major VALUES(1004,'CS');
INSERT INTO major VALUES(1006,'CS');
INSERT INTO major VALUES(1007,'CS');
INSERT INTO major VALUES(1007,'Physics');
INSERT INTO major VALUES(1008,'Physics');
INSERT INTO major VALUES(1009,'Biology');
INSERT INTO major VALUES(1010,'Biology');
INSERT INTO major VALUES(1011,'CS');
INSERT INTO major VALUES(1011,'Math');
INSERT INTO major VALUES(1012,'CS');
INSERT INTO major VALUES(1013,'CS');
INSERT INTO major VALUES(1013,'Psychology');
INSERT INTO major VALUES(1014,'Theater');
INSERT INTO major VALUES(1017,'Anthropology');





-- Problem 1
\echo '\n\nProblem 1'
SELECT Bo.BookNo, Bo.Title
FROM book Bo
WHERE Bo.Price >= 10 AND Bo.Price <=40 AND Bo.BookNo in
	(SELECT Bu.BookNo
	FROM buys Bu
	WHERE Bu.Sid in
		((SELECT M.Sid
		FROM major M
		WHERE M.Major = 'CS')
		INTERSECT
		(SELECT M.Sid
		FROM major M
		WHERE M.Major = 'Math')))
ORDER BY Bo.BookNo;
		 

-- Problem 2
\echo '\n\nProblem 2'
SELECT S.Sid, S.Sname
FROM student S
WHERE S.Sid in
	(SELECT Bu.Sid
	FROM buys Bu
	WHERE Bu.BookNo in
		(SELECT C.CitedBookNo
		FROM cites C
		WHERE (C.BookNo, C.CitedBookNo) in
			(SELECT Bo1.BookNo, Bo2.BookNo
			FROM book Bo1, book Bo2
			WHERE Bo1.Price > Bo2.Price)))
ORDER BY S.Sid;


-- Problem 3
\echo '\n\nProblem 3'
SELECT B.BookNo
FROM book B
WHERE B.BookNo in
	(SELECT C1.BookNo
	FROM cites C1, cites C2
	WHERE C1.CitedBookNo = C2.CitedBookNo AND C1.BookNo <> C2.BookNo)
ORDER BY B.BookNo;


--Problem 4
\echo '\n\nProblem 4'
SELECT B.BookNo
FROM book B
WHERE B.BookNo NOT IN
	(SELECT Bu.BookNo
	FROM buys Bu)
ORDER BY B.BookNo;


-- Problem 5
\echo '\n\nProblem 5'
SELECT S.Sid
FROM student S
WHERE S.Sid NOT IN
	(SELECT Bu.Sid
	FROM buys Bu
	WHERE Bu.BookNo IN
		(SELECT Bo.BookNo
		FROM book Bo
		WHERE Bo.Price > 50))
ORDER BY S.Sid;


-- Problem 6
\echo '\n\nProblem 6'
(SELECT Bu.BookNo
FROM buys Bu
WHERE Bu.Sid IN
	(SELECT M.Sid
	FROM major M
	WHERE M.Major = 'CS'))
EXCEPT
(SELECT Bu.BookNo
FROM buys Bu
WHERE Bu.Sid IN
	(SELECT M.Sid
	FROM major M
	WHERE M.Major = 'Math'))
ORDER BY 1;


-- Problem 7
\echo '\n\nProblem 7'
SELECT S.Sid, S.Sname
FROM student S
WHERE (S.Sid in
	(SELECT M1.Sid
	FROM major M1
	WHERE M1.Sid NOT IN
		(SELECT M2.Sid
		FROM major M2
		WHERE M1.Sid = M2.Sid AND M1.Major <> M2.Major)))
	AND (S.Sid NOT IN
	(SELECT Bu.Sid
	FROM buys Bu
	WHERE Bu.BookNo NOT IN
		(SELECT C.BookNo
		FROM cites C)));

-- Problem 8
\echo '\n\nProblem 8'
SELECT M.Sid, M.Major
FROM major M
WHERE M.Sid NOT IN
	(SELECT Bu.Sid
	FROM buys Bu
	WHERE Bu.BookNo IN 
		(SELECT Bo.BookNo
		FROM book Bo
		WHERE Bo.Price < 30));


-- Problem 9
\echo '\n\nProblem 9'
SELECT Bu.Sid, Bu.BookNo
FROM buys Bu
WHERE Bu.BookNo IN
	(SELECT Bo.BookNo
	FROM book Bo
	WHERE Bo.Price >= all(
		SELECT Bo1.Price
		FROM book Bo1
		WHERE Bo1.BookNo IN
			(SELECT Bu1.BookNo
			FROM buys Bu1
			WHERE Bu.Sid = Bu1.Sid)))
ORDER BY 1;


-- Problem 10
\echo '\n\nProblem 10'
(SELECT DISTINCT Bo2.Price
FROM book Bo2
WHERE NOT Bo2.Price < SOME(
	
	(SELECT DISTINCT Bo1.Price
	FROM book Bo1)
	EXCEPT
	(SELECT DISTINCT Bo.Price
	FROM book Bo
	WHERE NOT Bo.Price < SOME(
		SELECT Bo1.Price
		FROM book Bo1)))
EXCEPT
(SELECT DISTINCT Bo.Price
	FROM book Bo
	WHERE NOT Bo.Price < SOME(
		SELECT Bo1.Price
		FROM book Bo1)));


-- Problem 11
\echo '\n\nProblem 11'
(Select s.Sid, b1.BookNo, b2.BookNo
FROM
	(Select *
	FROM student) s,
	(Select *
	FROM book) b1,
	(Select *
	FROM book) b2
WHERE b1.BookNo <> b2.BookNo)

EXCEPT

(Select s.Sid, b1.BookNo, b2.BookNo
FROM
	(Select *
	FROM student) s,
	(Select *
	FROM book) b1,
	(Select *
	FROM book) b2
WHERE	((s.Sid, b1.BookNo) IN
			(Select *
			FROM buys))
	AND ((s.Sid, b2.BookNo) NOT IN
			(Select *
			FROM buys)));

	



-- Problem 12
\echo '\n\nProblem 12'
(SELECT S.Sid
FROM student S)
EXCEPT
(SELECT DISTINCT Bu.Sid
FROM buys Bu
WHERE Bu.BookNo IN
	(SELECT C.CitedBookNo
	FROM cites C
	WHERE C.BookNo = 2001))
ORDER BY 1;


-- Problem 13
\echo '\n\nProblem 13'
(SELECT Bu.BookNo as b1, Bu2.BookNo as b2
	FROM buys Bu, buys Bu2
	WHERE (Bu.Sid = Bu2.Sid)
	AND (Bu.BookNo <> Bu2.BookNo)
	AND (BU.Sid in 
		(SELECT m.Sid
		FROM major m
		WHERE m.Major = 'CS')))

EXCEPT

(Select DISTINCT t1.b1, t1.b2
FROM
	(SELECT DISTINCT Bu.Sid, Bu.BookNo as b1, Bu2.BookNo as b2
	FROM buys Bu, buys Bu2
	WHERE Bu.Sid = Bu2.Sid AND Bu.BookNo <> Bu2.BookNo) t1,

	(SELECT DISTINCT Bu.Sid, Bu.BookNo as b1, Bu2.BookNo as b2
	FROM buys Bu, buys Bu2
	WHERE Bu.Sid = Bu2.Sid AND Bu.BookNo <> Bu2.BookNo) t2

WHERE 	((t1.b1, t1.b2) = (t2.b1, t2.b2))
	AND (t1.Sid <> t2.Sid)
	AND (t1.SID in 
		(SELECT m.Sid
		FROM major m
		WHERE m.Major = 'CS'))
	AND (t2.SID in 
		(SELECT m.Sid
		FROM major m
		WHERE m.Major = 'CS')))
ORDER BY b1,b2;
	




-- Problem 14
\echo '\n\nProblem 14'
(SELECT Bu.Sid
FROM buys Bu)

EXCEPT

(SELECT Bu.Sid
FROM buys Bu
WHERE Bu.BookNo IN
	(SELECT Bo.BookNo
	FROM book Bo
	WHERE Bo.Price <= some
		(SELECT Bo.Price
		FROM book Bo
		WHERE NOT EXISTS(SELECT M.Sid
				FROM major M
				WHERE (M.Major = 'Math')
				AND M.Sid NOT IN 
					(SELECT Bu.Sid
					FROM buys Bu
					WHERE Bo.BookNo = Bu.BookNo)))));



DROP TABLE buys;
DROP TABLE cites;
DROP TABLE book;
DROP TABLE major;
DROP TABLE student;
