-- Data for Assignment 5 

CREATE TABLE student(Sid Integer, Sname VARCHAR(15), PRIMARY KEY(Sid));
CREATE TABLE major(Sid Integer REFERENCES student(Sid), Major VARCHAR(15), PRIMARY KEY(Sid, Major));
CREATE TABLE book(BookNo Integer, Title VARCHAR(30), Price Integer, PRIMARY KEY(BookNo));
CREATE TABLE cites(BookNo Integer REFERENCES book(BookNo), CitedBookNo Integer REFERENCES book(BookNo), PRIMARY KEY(BookNo, CitedBookNo));
CREATE TABLE buys(Sid Integer REFERENCES student(Sid), BookNo Integer REFERENCES book(BookNo), PRIMARY KEY(Sid, BookNo));

insert into student values(1001,'Jean');
insert into student values(1002,'Maria');
insert into student values(1003,'Anna');
insert into student values(1004,'Chin');
insert into student values(1005,'John');
insert into student values(1006,'Ryan');
insert into student values(1007,'Catherine');
insert into student values(1008,'Emma');
insert into student values(1009,'Jan');
insert into student values(1010,'Linda');
insert into student values(1011,'Nick');
insert into student values(1012,'Eric');
insert into student values(1013,'Lisa');
insert into student values(1014,'Filip');
insert into student values(1015,'Dirk');
insert into student values(1016,'Mary');
insert into student values(1017,'Ellen');
insert into student values(1020,'Ahmed');
insert into student values(1021,'Vince');
insert into student values(1022,'Joeri');
insert into student values(1023,'Chris');
insert into student values(1040,'Sofie');


-- book 

insert into book values(2001,'Databases',40);
insert into book values(2002,'OperatingSystems',25);
insert into book values(2003,'Networks',20);
insert into book values(2004,'AI',45);
insert into book values(2005,'DiscreteMathematics',20);
insert into book values(2006,'SQL',25);
insert into book values(2007,'ProgrammingLanguages',15);
insert into book values(2008,'DataScience',50);
insert into book values(2009,'Calculus',10);
insert into book values(2010,'Philosophy',25);
insert into book values(2012,'Geometry',80);
insert into book values(2013,'RealAnalysis',35);
insert into book values(2011,'Anthropology',50);
insert into book values(2014,'Topology',70);

-- cites 
insert into cites values(2012,2001);
insert into cites values(2008,2011);
insert into cites values(2008,2012);
insert into cites values(2001,2002);
insert into cites values(2001,2007);
insert into cites values(2002,2003);
insert into cites values(2003,2001);
insert into cites values(2003,2004);
insert into cites values(2003,2002);
insert into cites values(2010,2001);
insert into cites values(2010,2002);
insert into cites values(2010,2003);
insert into cites values(2010,2004);
insert into cites values(2010,2005);
insert into cites values(2010,2006);
insert into cites values(2010,2007);
insert into cites values(2010,2008);
insert into cites values(2010,2009);
insert into cites values(2010,2011);
insert into cites values(2010,2013);
insert into cites values(2010,2014);

-- buys
insert into buys values(1023,2012);
insert into buys values(1023,2014);
insert into buys values(1040,2002);
insert into buys values(1001,2002);
insert into buys values(1001,2007);
insert into buys values(1001,2009);
insert into buys values(1001,2011);
insert into buys values(1001,2013);
insert into buys values(1002,2001);
insert into buys values(1002,2002);
insert into buys values(1002,2007);
insert into buys values(1002,2011);
insert into buys values(1002,2012);
insert into buys values(1002,2013);
insert into buys values(1003,2002);
insert into buys values(1003,2007);
insert into buys values(1003,2011);
insert into buys values(1003,2012);
insert into buys values(1003,2013);
insert into buys values(1004,2006);
insert into buys values(1004,2007);
insert into buys values(1004,2008);
insert into buys values(1004,2011);
insert into buys values(1004,2012);
insert into buys values(1004,2013);
insert into buys values(1005,2007);
insert into buys values(1005,2011);
insert into buys values(1005,2012);
insert into buys values(1005,2013);
insert into buys values(1006,2006);
insert into buys values(1006,2007);
insert into buys values(1006,2008);
insert into buys values(1006,2011);
insert into buys values(1006,2012);
insert into buys values(1006,2013);
insert into buys values(1007,2001);
insert into buys values(1007,2002);
insert into buys values(1007,2003);
insert into buys values(1007,2007);
insert into buys values(1007,2008);
insert into buys values(1007,2009);
insert into buys values(1007,2010);
insert into buys values(1007,2011);
insert into buys values(1007,2012);
insert into buys values(1007,2013);
insert into buys values(1008,2007);
insert into buys values(1008,2011);
insert into buys values(1008,2012);
insert into buys values(1008,2013);
insert into buys values(1009,2001);
insert into buys values(1009,2002);
insert into buys values(1009,2011);
insert into buys values(1009,2012);
insert into buys values(1009,2013);
insert into buys values(1010,2001);
insert into buys values(1010,2002);
insert into buys values(1010,2003);
insert into buys values(1010,2011);
insert into buys values(1010,2012);
insert into buys values(1010,2013);
insert into buys values(1011,2002);
insert into buys values(1011,2011);
insert into buys values(1011,2012);
insert into buys values(1012,2011);
insert into buys values(1012,2012);
insert into buys values(1013,2001);
insert into buys values(1013,2011);
insert into buys values(1013,2012);
insert into buys values(1014,2008);
insert into buys values(1014,2011);
insert into buys values(1014,2012);
insert into buys values(1017,2001);
insert into buys values(1017,2002);
insert into buys values(1017,2003);
insert into buys values(1017,2008);
insert into buys values(1017,2012);
insert into buys values(1020,2001);
insert into buys values(1020,2012);
insert into buys values(1022,2014);

-- major
insert into major values(1001,'Math');
insert into major values(1001,'Physics');
insert into major values(1002,'CS');
insert into major values(1002,'Math');
insert into major values(1003,'Math');
insert into major values(1004,'CS');
insert into major values(1006,'CS');
insert into major values(1007,'CS');
insert into major values(1007,'Physics');
insert into major values(1008,'Physics');
insert into major values(1009,'Biology');
insert into major values(1010,'Biology');
insert into major values(1011,'CS');
insert into major values(1011,'Math');
insert into major values(1012,'CS');
insert into major values(1013,'CS');
insert into major values(1013,'Psychology');
insert into major values(1014,'Theater');
insert into major values(1017,'Anthropology');
insert into major values(1022,'CS');
insert into major values(1015,'Chemistry');


--Problem 1

create or replace function setunion(A anyarray, B anyarray) returns anyarray as
$$
with
	Aset as (select UNNEST(A)),
	Bset as (select UNNEST(B))
	select array( (select * from Aset) union (select * from Bset) order by 1);
$$ language sql;


create or replace function setintersection(A anyarray, B anyarray) returns anyarray as
$$
with
	Aset as (select UNNEST(A)),
	Bset as (select UNNEST(B))
	select array( (select * from Aset a where exists(select * from Bset b where a.* = b.*)) order by 1);
$$ language sql;

create or replace function setdifference(A anyarray, B anyarray) returns anyarray as
$$
with
	Aset as (select UNNEST(A)),
	Bset as (select UNNEST(B))
	select array( (select * from Aset) except (select * from Bset) order by 1);
$$ language sql;


--Problem 2

--given
create or replace view student_books as
	select s.sid, array(	select t.bookno
				from buys t
				where  t.sid = s.sid
				order by bookno) as books
	from student s order by sid;
select * from student_books;

--(a)
\echo
\echo
\echo Problem 2a
\echo ---------------------------------------
create or replace view book_students as
	select b.bookno, array(	select t.sid
				from buys t
				where  t.bookno = b.bookno
				order by bookno) as students
	from book b order by bookno;
select * from book_students;

--(b)
\echo
\echo
\echo Problem 2b
\echo ---------------------------------------
create or replace view book_citedbooks as
	select b.bookno, array(	select c.citedbookno
				from cites c
				where  c.bookno = b.bookno
				order by bookno) as citedbooks
	from book b order by bookno;
select * from book_citedbooks;


--(c)
\echo
\echo
\echo Problem 2c
\echo ---------------------------------------
create or replace view book_citingbooks as
	select b.bookno, array(	select c.bookno
				from cites c
				where  c.citedbookno = b.bookno
				order by bookno) as citingbooks
	from book b order by bookno;
select * from book_citingbooks;

--(d)
\echo
\echo
\echo Problem 2d
\echo ---------------------------------------
create or replace view major_students as
	select distinct m.major, array(	select m1.sid
				from major m1
				where  m1.major = m.major
				order by m1.sid) as students
	from major m order by major;
select * from major_students;


--(e)
\echo
\echo
\echo Problem 2e
\echo ---------------------------------------
create or replace view student_majors as
	select s.sid, array(	select m.major
				from major m
				where  m.sid = s.sid
				order by m.major) as majors
	from student s order by sid;
select * from student_majors;


--Problem 3

--(a)
\echo
\echo
\echo Problem 3a
\echo ---------------------------------------
select b.bookno
from book_citingbooks b
where 	cardinality(	setdifference(	b.citingbooks,
					array(select bookno
					from book
					where price >= 50)))
	 >= 2;



--(b)
\echo
\echo
\echo Problem 3b
\echo ---------------------------------------
select distinct b.bookno, b.title
from book b, student_books sb
where	b.bookno = any(sb.books)
	and 	2 = (	select count(*)
		 	from major_students
			where (major = 'CS' or major = 'Math') and sb.sid = any(students));

-- (c)
\echo
\echo
\echo Problem 3c
\echo ---------------------------------------
select b.bookno
from book_citingbooks b
where cardinality(citingbooks) = 1;


-- (d)
\echo
\echo
\echo Problem 3d
\echo ---------------------------------------
select s.sid
from student_books s
where 	cardinality(	setdifference( array(select bookno
					from book
					where price > 50),
					s.books))
	 = 0;

-- (e)
\echo
\echo
\echo Problem 3e
\echo ---------------------------------------
select s.sid
from student_books s
where 	cardinality(	setintersection( array(select bookno
					from book
					where price > 50),
					s.books))
	 = 0;

-- (f)
\echo
\echo
\echo Problem 3f
\echo ---------------------------------------
select s.sid
from student_books s
where 	cardinality(	setintersection( array(select bookno
					from book
					where price <= 50),
					s.books))
	 = 0;

-- (g)
\echo
\echo
\echo Problem 3g
\echo ---------------------------------------
select s.sid
from student_books s
where 	cardinality(	setintersection( array(select bookno
					from book
					where price < 50),
					s.books))
	 = 1;


-- (h)
\echo
\echo
\echo Problem 3h
\echo ---------------------------------------
select b.bookno
from book b
where
	(select count(*)
	from student_books s
	where 	exists( select *
			from major_students
			where major = 'CS' and s.sid = any(students))
		and b.bookno = any(s.books)) = 0;


-- (i)
\echo
\echo
\echo Problem 3i
\echo ---------------------------------------
select b.bookno
from book b
where
	(select count(*)
	from student_books s
	where 	exists( select *
			from major_students
			where major = 'Anthropology' and s.sid = any(students))
		and b.bookno = any(s.books))
	
	< (select cardinality(students) from major_students where major = 'Anthropology');



-- (j)
\echo
\echo
\echo Problem 3j
\echo ---------------------------------------
select sb.sid, b.bookno
from student_books sb, book_citingbooks b
where not sb.books <@ b.citingbooks;


-- (k)
\echo
\echo
\echo Problem 3k
\echo ---------------------------------------
select sb.sid, b.bookno
from student_books sb, book_citingbooks b
where sb.books <@ b.citingbooks;

--(l)
\echo
\echo
\echo Problem 3l
\echo ---------------------------------------
select sb1.sid, sb2.sid
from student_books sb1, student_books sb2
where sb1.sid <> sb2.sid and sb1.books <@ sb2.books and sb1.books @> sb2.books;

--(m)
\echo
\echo
\echo Problem 3m
\echo ---------------------------------------
select sb1.sid, sb2.sid
from student_books sb1, student_books sb2
where sb1.sid <> sb2.sid and cardinality(sb1.books) = cardinality(sb2.books);

--(n)
\echo
\echo
\echo Problem 3n
\echo ---------------------------------------
select b.bookno
from book_citedbooks b
where 	cardinality(	setdifference(	array(select bookno
					from book),
					b.citedbooks))

	 = 2;


DROP TABLE buys CASCADE;
DROP TABLE cites CASCADE;
DROP TABLE book CASCADE;
DROP TABLE major CASCADE;
DROP TABLE student CASCADE;

